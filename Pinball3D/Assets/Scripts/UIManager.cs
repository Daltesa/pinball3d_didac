﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public Text lives;
	public Text score;
	public Text multiplicator;

	public GameObject startText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void showStartText(bool flag){
		startText.SetActive (flag);
	}

	public void drawLives(string _lives){
		lives.text = "Lives: " + _lives;
	}
	public void drawScore(string _score){
		score.text ="Score: " + _score;
	}
	public void drawMultiplicator(string _multiplicator){
		multiplicator.text ="x"  + _multiplicator;
	}
}
