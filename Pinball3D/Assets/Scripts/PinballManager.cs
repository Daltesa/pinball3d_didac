﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PinballManager : MonoBehaviour {

	public static PinballManager Instance;
	public int maxLives;
	public GameObject ballSpawnPoint;
	public GameObject ballPrefab;


	public UIManager uiManager;
	public SoundManager soundManger;

	private GameObject ingameBall;
	private int multiplicator = 1;
	private bool inGame = false;

	public int actualLives;
	public int actualScore = 0;

	public Transform[] HumanTargetPoints = new Transform[5];
	// Use this for initialization
	void Awake () {
		DontDestroyOnLoad (this);
		uiManager.showStartText (true);
		Instance = this;
		actualLives = maxLives;
		actualScore = 0;
		resetGame ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.anyKey && !inGame) {
			uiManager.showStartText (false);
			ingameBall = Instantiate (ballPrefab, ballSpawnPoint.transform.position, ballSpawnPoint.transform.rotation);
			resetGame ();
			inGame = true;
		}
	}
	private void resetGame(){
		actualLives = maxLives;
		multiplicator = 1;
		actualScore = 0;
		uiManager.drawScore(actualScore.ToString());
		uiManager.drawMultiplicator (multiplicator.ToString());
		uiManager.drawLives (actualLives.ToString());

	}

	public void UpdateScore(int score){
		actualScore += score * multiplicator;
		uiManager.drawScore(actualScore.ToString());
	}

	public void MultiplyScore(){
		if (multiplicator <= 1) {
			multiplicator = 3;
		} else {
			multiplicator += 3;
		}
		uiManager.drawMultiplicator (multiplicator.ToString());
	}

	public void UpdateLives(int lives){
		actualLives -= lives;
		GameOverCheck ();
		uiManager.drawLives (actualLives.ToString());
	}


	private void GameOverCheck(){
		if(actualLives <= 0){
			uiManager.showStartText (true);
			Destroy (ingameBall);
			inGame = false;
			//SceneManager.LoadScene (0);

		}
	}
}
