﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public AudioSource fxSource;

	public AudioClip music;
	public AudioClip bumperSound;
	public AudioClip flipperSound;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void PlayBumpSound(){
		fxSource.clip = bumperSound;
		fxSource.Play ();
	}
	public void PlayFlipperSound(){
		fxSource.clip = flipperSound;
		fxSource.Play ();
	}
}
