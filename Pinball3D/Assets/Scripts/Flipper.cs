﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipper : MonoBehaviour {
	public float initialAngle;
	public float maxAngle;
	public float flipperForce;
	public float flipperDamper;
	public string InputName;

	private HingeJoint hJoint;
	private bool flipperPressed;

	//****************************************************************************************************************START
	void Start () {
		InitializeHingeJoin ();
	}
	
	//****************************************************************************************************************UPDATE
	void Update () {
		GetInput ();
		MoveFlipper ();
	}
	//****************************************************************************************************************MOVE FLIPPER
	void MoveFlipper(){

		JointSpring newSpring = new JointSpring ();
		newSpring.spring = flipperForce;
		newSpring.damper = flipperDamper;

		if (flipperPressed) {
			PinballManager.Instance.soundManger.PlayFlipperSound ();
			newSpring.targetPosition = maxAngle;
		} else {
			newSpring.targetPosition = initialAngle;
		}
		hJoint.spring = newSpring;

	}
	//****************************************************************************************************************GET INPUT
	void GetInput(){
		if (Input.GetAxis(InputName) == 1) {
			
			flipperPressed = true;
		} else {
			flipperPressed = false;
		}
	}
	//****************************************************************************************************************INITIALIZE JOINT
	void InitializeHingeJoin(){
		hJoint = GetComponent<HingeJoint> ();
		hJoint.useSpring = true;

		//setUpLimits
		JointLimits limits = hJoint.limits;
		limits.min = -maxAngle;
		limits.max = maxAngle;
		hJoint.limits = limits;
		hJoint.useLimits = true;

	}
}
