﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HumanScript : MonoBehaviour {

	public GameObject ragdoll;
	public ParticleSystem blood;
	public GameObject bloodparent;



	public Transform destination;

	NavMeshAgent navMesh;
	// Use this for initialization
	void Start () {
		navMesh = GetComponent<NavMeshAgent> ();
		setNewTarget ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Vector3.Distance(transform.position,destination.transform.position) < 0.2f) {
			setNewTarget ();
		}
		//SpawnParticles ();
	}

	void setNewTarget(){
		int random = Random.Range (0, 15);
		destination = PinballManager.Instance.HumanTargetPoints [random];
		setDestination ();

	}

	void setDestination() {
		if (destination != null) {
			Vector3 targetVetcor = destination.transform.position;
			navMesh.SetDestination (targetVetcor);

		}
	}

	void  OnTriggerEnter(Collider col) {
		if (col.gameObject.tag == "Ball") {
			SpawnParticles ();
			ActivateRagdoll ();
			Destroy (this.gameObject);

		}
	}



	void ActivateRagdoll(){
		Instantiate(ragdoll, transform.position, transform.rotation, transform.parent);
	}
	void SpawnParticles(){
		ParticleSystem p = Instantiate(blood, new Vector3(transform.position.x,transform.position.y +0.3f, transform.position.z),transform.rotation);
		p.name = blood.name;
		p.Play();
	}
}
