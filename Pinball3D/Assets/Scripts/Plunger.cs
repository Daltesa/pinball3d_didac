﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plunger : MonoBehaviour {
	//public
	public float maxForce;
	public float maxZ = -1f;
	//private
	private bool charging;
	private float actualForce;

	private float initialPos;
	private float limitPos;
	private Vector3 initvector;
	private Rigidbody myRigidbody;

	void Start () {
		
		myRigidbody = transform.GetComponent<Rigidbody> ();
		initialPos = transform.position.z;
		initvector = transform.position;
		limitPos = transform.position.z - maxZ;
	}

	// Update is called once per frame
	void Update () {
		CheckInput ();
		if (charging) {
			if (transform.position.z > limitPos) {
				MoveToLimit ();
				if (actualForce < maxForce) {
					actualForce += 150;
				}
			}
		} else {
			if (transform.position.z > initialPos) {
				transform.position = initvector;
				myRigidbody.velocity = new Vector3(0,0,0);

			}
		}

	}
	void MoveToLimit(){
		transform.Translate ( -Vector3.forward  * Time.deltaTime);
	}
	void MoveToInitial(){
		transform.Translate (Vector3.forward * Time.deltaTime * 20);
	}

	void CheckInput(){
		//Space
		if(Input.GetButtonDown("Plunger")){
			Debug.Log ("pressed");
			charging = true;
		}
		if(Input.GetButtonUp("Plunger")){
			Debug.Log ("pressed");
			charging = false;
			Impulse ();
		}
	}

//	void OnCollisionEnter(Collision col){
//		if (col.gameObject.tag == "Ball") {
//			myRigidbody = col.transform.GetComponent<Rigidbody> ();
//		}
//	}
	void Impulse(){
		Debug.Log (actualForce);
		myRigidbody.AddRelativeForce(Vector3.forward * actualForce,ForceMode.Impulse);
		actualForce = 0;


	
	}
}
