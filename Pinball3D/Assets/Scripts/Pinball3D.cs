﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pinball3D : MonoBehaviour {
	public int rotationAngle;

	void Awake(){
		transform.Rotate (new Vector3 (-rotationAngle, 0, 0));
	}
}
