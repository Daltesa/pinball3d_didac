﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {
	
	//********************PUBLIC
	public bool activated = false;

	//********************PRIVATE
	private TargetsManager targetsManager;

	//**************************************************************************************************START
	void Start () {
		targetsManager = transform.GetComponentInParent<TargetsManager> ();
	}
	
	//**************************************************************************************************UPDATE
	void Update () {
		
	}

	//**************************************************************************************************TARGET HITTED
	void TargetHitted(){
		activated = true;
		targetsManager.newTargetHit (this.gameObject);
	}

	//**************************************************************************************************ON COLLISION ENTER
	void OnCollisionEnter(Collision col){
		if (col.gameObject.tag == "Ball" && activated == false) {
			Debug.Log ("Impact");
			TargetHitted ();
		}
	
	}
}
