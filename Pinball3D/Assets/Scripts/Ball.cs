﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator spawn(){
		yield return new WaitForSecondsRealtime (2);
		transform.position = PinballManager.Instance.ballSpawnPoint.transform.position;
	}

	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "Dead") {
			PinballManager.Instance.UpdateLives (1);
			StartCoroutine ("spawn");

		}
	}
}
