﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanSpawner : MonoBehaviour {
	public GameObject human;
	public float minTime;
	public float maxTime;
	// Use this for initialization
	void Start () {
		generateRandomTime ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void generateRandomTime(){
		float time = Random.Range (minTime, minTime);
		StartCoroutine (spawnIn (time));
	}

	private IEnumerator spawnIn(float time){
		yield return new WaitForSecondsRealtime (time);
		spawnHuman ();
	}

	private void spawnHuman(){
		Instantiate (human,transform.position, transform.rotation);
		generateRandomTime ();
	}
}
