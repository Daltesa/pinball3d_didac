﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyIn : MonoBehaviour {
	public float secondsUntilDestroy;

	// Use this for initialization
	void Start () {
		StartCoroutine (destroyIn (secondsUntilDestroy));
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator destroyIn(float secs){
		yield return new WaitForSecondsRealtime (secs);
	}
}
