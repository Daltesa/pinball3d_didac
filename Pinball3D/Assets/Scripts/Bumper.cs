﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour {
	
	//********************PUBLIC
	public float bumperForce = 10;

	private GameObject lights;

	//**************************************************************************************************START
	void Start () {
		lights = transform.GetChild (0).gameObject;
		
	}
	
	//**************************************************************************************************UPDATE
	void Update () {
		
	}
	public IEnumerator turnOffLights(){
		yield return new WaitForSecondsRealtime(0.2f);
		lights.SetActive (false);
	
	}

	//************************************************************************************************** ON COLLISION ENTER
	void  OnCollisionEnter(Collision col) {
		if (col.gameObject.tag == "Ball") {
			PinballManager.Instance.UpdateScore (10);
			PinballManager.Instance.soundManger.PlayBumpSound ();
			StartCoroutine("turnOffLights");
			lights.SetActive (true);
			foreach (ContactPoint contact in col.contacts) {
				
				contact.otherCollider.GetComponent<Rigidbody>().AddForce( -1 * contact.normal * bumperForce,  ForceMode.Impulse);
				
			}
		}
	}
}
