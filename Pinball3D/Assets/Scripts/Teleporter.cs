﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour {

	public GameObject destination;

	private GameObject ball;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator teleportBall(){
		yield return new WaitForSeconds (2);
		ball.transform.position = destination.transform.position;
		ball.SetActive (true);
		ball.GetComponent<Rigidbody> ().velocity = new Vector3(0,0,0);
	}

	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "Ball") {
			ball = col.gameObject;
			ball.SetActive (false);
			StartCoroutine ("teleportBall");
		
		}
	}

}
