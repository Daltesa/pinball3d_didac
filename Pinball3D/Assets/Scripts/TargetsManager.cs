﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetsManager: MonoBehaviour {
	
	//********************PUBLIC
	public int targetsForBonus;

	//********************PRIVATE
	private List<GameObject> targets = new List<GameObject>();
	private int actualTargetsForBonus;

	//**************************************************************************************************START
	void Start () {
		actualTargetsForBonus = targetsForBonus;
	}

	//**************************************************************************************************UPDATE
	void Update () {
		
	}

	//**************************************************************************************************ACTIVATE BOONUS
	private void activeBonus(){
		PinballManager.Instance.MultiplyScore ();
		StartCoroutine ("EnableTargets");
	}

	//**************************************************************************************************ENABLE TARGETS
	public IEnumerator EnableTargets(){
		yield return new WaitForSecondsRealtime (2);
		for (int i = 0; i < targets.Count; i++) {
			targets [i].SetActive (true);
			targets [i].GetComponent<Target> ().activated = false;
		}
	}

	//**************************************************************************************************NEW TARGET HITT
	public void newTargetHit(GameObject aTarget){
		Debug.Log (aTarget);
		targets.Add (aTarget);
		aTarget.SetActive (false);
		actualTargetsForBonus--;
		if (actualTargetsForBonus <= 0) {
			actualTargetsForBonus = 3;
			activeBonus();
		}
	}
}
